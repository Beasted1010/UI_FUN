

#ifndef COMMON_H
#define COMMON_H

#include "stdio.h"

#define SCREEN_WIDTH (1280)
#define SCREEN_HEIGHT (720)

#define TEST( string ) puts(string "\n")

//########################### MENU ###############################

#define MENUWIDTH 10
#define MENUHEIGHT 10

//########################### END ################################

#ifdef DEBUG

#define applog(...) printf(__VA_ARGS__); printf("\n");

#endif

#endif
