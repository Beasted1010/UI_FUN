

#ifndef MENU_H
#define MENU_H

#include "SDL.h"
#include "Font.h"

// TODO: MAKE THIS DYNAMIC
#define NUMBUTTONS 6
#define NUMCHECKBOXES 12
#define NUMSLIDERS 4

typedef enum MouseStateEnum
{
    MouseEventUp,
    MouseEventDown,
    MouseUp,
    MouseDown
} MouseState;

typedef enum CellAlignmentEnum
{
    Left,
    Right,
    Up = Left,
    Down
} CellAlignment;

// TODO: These values are the same as above... Might as well get rid of this :D
// TODO: Currently setting enum value to 50000, to make it unique.... Keep this?
typedef enum SliderStartValsEnum
{
    FarLeft = 0, // Far left is default for horizontal sliders
    Top = 0, // Top is default for vertical sliders
    FarRight = -1,
    Bottom = -1
}SliderStartVals;

typedef enum sliderOrientationEnum
{
    Horizontal, // Horizontal (long ways: left to right) is default
    Vertical
}SliderOrientation;

typedef struct WidgetStateStruct
{
    int isHovered;
    int isSelected;
}WidgetState;

typedef struct GridLocStruct
{
    int x;
    int y;
    int w;
    int h;
}GridLoc;

typedef struct ButtonStruct
{
    int cellsWide;
    int cellsHigh;

    char label[64];
    int fontSize;

    SDL_Rect buttonRect;

    GridLoc gridStartLoc;

    WidgetState state;
}Button;

typedef struct CheckboxStruct
{
    int cellsWide;
    int cellsHigh;

    int isChecked;

    int alignment;

    SDL_Rect checkboxRect;

    GridLoc gridStartLoc;

    WidgetState state;

}Checkbox;

typedef struct SliderStruct
{
    //TODO: this should be discarded and a better idea should be used
    int defaultNeedsDrawn;

    // Determine if vertical or straight up
    int sliderOrientation;

    // Determine the alignment of the slider within the cell (Left/Right)
    int alignment;

    // How many cells the length of the slider stretches across
    int cellsSpan_Bar;

    // The value at which the slider starts
    int sliderStartVal;

    // The number of possible slider values
    int numValues;

    // Not used
    //int sliderValue;

    SDL_Rect barRect;
    SDL_Rect sliderRect;

    // The location on grid for the entire bar
    GridLoc gridStartLoc;

    WidgetState state;

}Slider;

// TODO: Implement this
typedef struct WidgetStruct
{
    Button* button;
    Checkbox* checkbox;
    Slider* slider;
}Widget;

typedef struct GridStruct
{
    int numCells;
    int cellWidth_px;
    int cellHeight_px;
}Grid;

typedef struct MenuStruct
{
    Button button[NUMBUTTONS];
    Checkbox checkbox[NUMCHECKBOXES];
    Slider slider[NUMSLIDERS];

    Grid* grid;
    FNT_Font* font;

    // TODO: Perhaps have a Widgets struct that contains all the arrays, because a menu should only know about widgets, which
    //are what we desire.
    int numButtons;
    int numCheckboxes;
    int numSliders;

    int cellWidth;
    int cellHeight;
}Menu;

// TODO: Optimize these (if no changes made, then const * it)
Menu* CreateMenu( int cellWidth, int cellHeight, FNT_Font* font );

void AddButton( Menu* menu, int cellsWide, int cellsHigh, int gridLocx,
                                              int gridLocy, char* text );

void AddCheckbox( Menu* menu, int sideCellLength, int gridLoc_x, int gridLoc_y,
                                                                 int alignment );

// TODO: Make this name for "CellsWide" more general for veritcal sliders as well
void AddSlider( Menu* menu, int cellsSpan_Bar, int numValues, int sliderStartVal,
               int gridLoc_x, int gridLoc_y, int sliderOrientation, int alignment );

void UpdateMenu( Menu* menu, int mousex, int mousey, MouseState mouseState );

void DisplayMenu( SDL_Renderer* rend, Menu* menu, int showGrid );

void DestroyMenu( Menu* menu );

void DrawGrid( Menu* menu, SDL_Renderer* rend );

#endif
