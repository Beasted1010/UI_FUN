
#include "Menu.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "Font.h"

//#include "SDL.h"

#include "AppState.h"

Menu* CreateMenu( int cellWidth, int cellHeight, FNT_Font* font )
{
    Menu* menu = malloc( sizeof(Menu) );

    menu->font = font;
    menu->cellWidth = cellWidth;
    menu->cellHeight = cellHeight;
    menu->numButtons = 0;
    menu->numCheckboxes = 0;
    menu->numSliders = 0;

    // TODO: MAKE THIS NOT THIS
    menu->grid = malloc( sizeof(Grid) );

    menu->grid->numCells = menu->cellWidth * menu->cellHeight;
    menu->grid->cellWidth_px = SCREEN_WIDTH / menu->cellWidth;
    menu->grid->cellHeight_px = SCREEN_HEIGHT / menu->cellHeight;

#ifdef DEBUG
    applog("Created a Menu...");
#endif

    return menu;
}

void DrawGrid( Menu* menu, SDL_Renderer* rend )
{
    SDL_SetRenderDrawColor( rend, 125, 125, 125, SDL_ALPHA_OPAQUE );
    int i;
    for( i = 1; i < menu->cellHeight; i++ )
    {
        SDL_RenderDrawLine( rend, 0, i * menu->grid->cellHeight_px, SCREEN_WIDTH, i * menu->grid->cellHeight_px );
    }

    for( i = 1; i < menu->cellWidth; i++ )
    {
        SDL_RenderDrawLine( rend, i * menu->grid->cellWidth_px, 0, i * menu->grid->cellWidth_px, SCREEN_HEIGHT );
    }
}

void AddButton( Menu* menu, int cellsWide, int cellsHigh,
                int gridLoc_x, int gridLoc_y, char* label )
{
    // Set dimensions for our button
    menu->button[menu->numButtons].cellsHigh = cellsHigh;
    menu->button[menu->numButtons].cellsWide = cellsWide;

    strncpy( menu->button[menu->numButtons].label, label, sizeof(menu->button[menu->numButtons].label) - 1 );
    menu->button[menu->numButtons].fontSize = 20;

    // Set the starting cell for our button
    menu->button[menu->numButtons].gridStartLoc.x = gridLoc_x;
    menu->button[menu->numButtons].gridStartLoc.y = gridLoc_y;
    menu->button[menu->numButtons].gridStartLoc.w = menu->button[menu->numButtons].cellsWide;
    menu->button[menu->numButtons].gridStartLoc.h = menu->button[menu->numButtons].cellsHigh;

    // Initialize button state information
    menu->button[menu->numButtons].state.isHovered = 0;
    menu->button[menu->numButtons].state.isSelected = 0;

    menu->numButtons++;
}

void AddCheckbox( Menu* menu, int sideCellLength, int gridLoc_x,
                                   int gridLoc_y, int alignment)
{
    // Set dimensions for our checkbox
    menu->checkbox[menu->numCheckboxes].cellsHigh = sideCellLength;
    menu->checkbox[menu->numCheckboxes].cellsWide = sideCellLength;

    menu->checkbox[menu->numCheckboxes].alignment = alignment;

    // Set the starting cell for our checkbox
    menu->checkbox[menu->numCheckboxes].gridStartLoc.x = gridLoc_x;
    menu->checkbox[menu->numCheckboxes].gridStartLoc.y = gridLoc_y;
    menu->checkbox[menu->numCheckboxes].gridStartLoc.w = menu->checkbox[menu->numCheckboxes].cellsWide;
    menu->checkbox[menu->numCheckboxes].gridStartLoc.h = menu->checkbox[menu->numCheckboxes].cellsHigh;

    // Initialize checkbox state information
    menu->checkbox[menu->numCheckboxes].state.isHovered = 0;
    menu->checkbox[menu->numCheckboxes].state.isSelected = 0;
    menu->checkbox[menu->numCheckboxes].isChecked = 0;

    menu->numCheckboxes++;
}

void AddSlider( Menu* menu, int cellsSpan_Bar, int numValues, int sliderStartVal,
                int gridLoc_x, int gridLoc_y, int sliderOrientation, int alignment )
{
    menu->slider[menu->numSliders].cellsSpan_Bar = cellsSpan_Bar;

    menu->slider[menu->numSliders].gridStartLoc.x = gridLoc_x;
    menu->slider[menu->numSliders].gridStartLoc.y = gridLoc_y;

    // The bar will always be the same height, a quarter of the cell's height
    // TODO: Maybe?
    if( sliderOrientation )
    {
        menu->slider[menu->numSliders].gridStartLoc.w = 1;
        menu->slider[menu->numSliders].gridStartLoc.h = menu->slider[menu->numSliders].cellsSpan_Bar;
    }
    else
    {
        menu->slider[menu->numSliders].gridStartLoc.w = menu->slider[menu->numSliders].cellsSpan_Bar;
        menu->slider[menu->numSliders].gridStartLoc.h = 1;
    }

    menu->slider[menu->numSliders].sliderOrientation = sliderOrientation;

    menu->slider[menu->numSliders].alignment = alignment;

    menu->slider[menu->numSliders].numValues = numValues;

    menu->slider[menu->numSliders].sliderStartVal = sliderStartVal;

    menu->slider[menu->numSliders].state.isHovered = 0;
    menu->slider[menu->numSliders].state.isSelected = 0;

    menu->slider[menu->numSliders].defaultNeedsDrawn = 1;

    menu->numSliders++;
}

static inline void DrawButtons( SDL_Renderer* rend, Menu* menu, int xoff, int yoff)
{
    int i;
    for(i = 0; i < menu->numButtons; i++)
    {
        // TODO: This buttonRect drawing modification should be in draw the buttons function
        menu->button[i].buttonRect.x = menu->button[i].gridStartLoc.x * menu->grid->cellWidth_px + xoff;
        menu->button[i].buttonRect.y = menu->button[i].gridStartLoc.y * menu->grid->cellHeight_px + yoff;

        menu->button[i].buttonRect.w = menu->button[i].gridStartLoc.w * menu->grid->cellWidth_px - 2 * xoff;
        menu->button[i].buttonRect.h = menu->button[i].gridStartLoc.h * menu->grid->cellHeight_px - 2 * yoff;

        if( !menu->button[i].state.isSelected && !menu->button[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 255, 0, 0, SDL_ALPHA_OPAQUE );
        }
        else if( menu->button[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 0, 255, 0, SDL_ALPHA_OPAQUE );
        }
        else if( menu->button[i].state.isSelected )
        {
            SDL_SetRenderDrawColor( rend, 0, 0, 255, SDL_ALPHA_OPAQUE );
        }
        FNT_DrawTextRect( rend, menu->font, menu->button[i].label, menu->button[i].buttonRect,
                         0, 0, menu->button[i].fontSize,  FNT_ALIGNCENTERX | FNT_ALIGNCENTERY );
        SDL_RenderDrawRect( rend, &menu->button[i].buttonRect );
    }
}

static inline void DrawCheckboxes( SDL_Renderer* rend, Menu* menu, int xoff, int yoff )
{
    int i;
    int cellWidth = menu->grid->cellWidth_px;
    int cellHeight = menu->grid->cellHeight_px;
    int squareOrientation = cellWidth;
    int save = xoff;
    for(i = 0; i < menu->numCheckboxes; i++)
    {
        if( cellWidth > cellHeight )
        {   squareOrientation = cellHeight; }

        menu->checkbox[i].checkboxRect.w = menu->checkbox[i].gridStartLoc.w * squareOrientation - 2 * xoff;
        menu->checkbox[i].checkboxRect.h = menu->checkbox[i].checkboxRect.w;

        // TODO: Make this pretty, and consistent with different menu sizes (the dependencees are non-consistent)
        if( menu->checkbox[i].alignment )
        { xoff += cellWidth - 4 * xoff; }
        menu->checkbox[i].checkboxRect.x = menu->checkbox[i].gridStartLoc.x * cellWidth + xoff;
        menu->checkbox[i].checkboxRect.y = menu->checkbox[i].gridStartLoc.y * cellHeight + yoff;

        if( !menu->checkbox[i].state.isSelected && !menu->checkbox[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 255, 0, 0, SDL_ALPHA_OPAQUE );
        }
        else if( menu->checkbox[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 0, 255, 0, SDL_ALPHA_OPAQUE );
        }
        else if( menu->checkbox[i].state.isSelected )
        {
            SDL_SetRenderDrawColor( rend, 0, 0, 255, SDL_ALPHA_OPAQUE );
            if( menu->checkbox[i].isChecked )
            { menu->checkbox[i].isChecked = 0; }
            else
            { menu->checkbox[i].isChecked = 1; }
        }
        SDL_RenderDrawRect( rend, &menu->checkbox[i].checkboxRect );

        // TODO: Seems to have a few bugs with checking a checkbox, acts funny. maybe?
        if( menu->checkbox[i].isChecked )
        {
            SDL_SetRenderDrawColor( rend, 255, 255, 255, SDL_ALPHA_OPAQUE );
            // TODO: Make the lines not be so ugly with the color of the checkbox (overlap)
            SDL_RenderDrawLine( rend, menu->checkbox[i].checkboxRect.x, menu->checkbox[i].checkboxRect.y,
                                      menu->checkbox[i].checkboxRect.x + menu->checkbox[i].checkboxRect.w,
                                      menu->checkbox[i].checkboxRect.y + menu->checkbox[i].checkboxRect.h );
            SDL_RenderDrawLine( rend, menu->checkbox[i].checkboxRect.x,
                                      menu->checkbox[i].checkboxRect.y + menu->checkbox[i].checkboxRect.h,
                                      menu->checkbox[i].checkboxRect.x + menu->checkbox[i].checkboxRect.w,
                                      menu->checkbox[i].checkboxRect.y );
        }
        xoff = save;
    }
}

static inline void DrawHorizontalSliderValues( SDL_Renderer* rend, int i, Menu* menu )
{
    int j;
    int xPos;
    int yPos;
    int height;
    for(j = 0; j < menu->slider[i].numValues; j++)
    {
        xPos = ( (menu->slider[i].barRect.w * j) / (menu->slider[i].numValues - 1) )
                                                      + menu->slider[i].barRect.x;
        height = menu->slider[i].barRect.h * 2;
        yPos = menu->slider[i].barRect.y - (height / 4);
        SDL_SetRenderDrawColor( rend, 100, 100, 100, SDL_ALPHA_OPAQUE );
        SDL_RenderDrawLine( rend, xPos, yPos, xPos, yPos + height );
    }
}

static inline void DrawVerticalSliderValues( SDL_Renderer* rend, int i, Menu* menu )
{
    int j;
    int xPos;
    int yPos;
    int width;
    for(j = 0; j < menu->slider[i].numValues; j++)
    {
        /*xPos = ( (menu->slider[i].barRect.w * j) / (menu->slider[i].numValues - 1) )
                                                      + menu->slider[i].barRect.x;*/
        yPos = ( (menu->slider[i].barRect.h * j) / (menu->slider[i].numValues - 1) )
                                                      + menu->slider[i].barRect.y;
        width = menu->slider[i].barRect.w * 2;
        //yPos = menu->slider[i].barRect.y - (height / 4);
        xPos = menu->slider[i].barRect.x - (width / 4);
        SDL_SetRenderDrawColor( rend, 100, 100, 100, SDL_ALPHA_OPAQUE );
        SDL_RenderDrawLine( rend, xPos, yPos, xPos + width, yPos );
    }
}

// TODO: MAYBE MAKE A SEPARATE FUNCTION FOR VERTICAL SLIDERS... Perhaps combine this with the slider value function
static inline void setDefaultHorizontalSliderPos( Menu* menu, int xoff, int yoff, int i )
{
    int cellWidth = menu->grid->cellWidth_px;
    int cellHeight = menu->grid->cellHeight_px;
    int save = yoff;
    menu->slider[i].barRect.w = cellWidth * menu->slider[i].gridStartLoc.w - 2 * xoff;
    // TODO: Keep this as a hard coded value, NOTE: gridStartLoc.h uses int only :D
    menu->slider[i].barRect.h = cellHeight * 0.2;

    // TODO: Make this pretty, and consistent with different menu sizes (the dependencees are non-consistent)
    if( menu->slider[i].alignment )
    { yoff += cellHeight - 3 * yoff; }

    menu->slider[i].barRect.x = menu->slider[i].gridStartLoc.x * cellWidth + xoff;
    menu->slider[i].barRect.y = menu->slider[i].gridStartLoc.y * cellHeight + yoff;

    menu->slider[i].sliderRect.w = 20; // Keep this value?
    menu->slider[i].sliderRect.h = menu->slider[i].barRect.h * 2;
    menu->slider[i].sliderRect.y = menu->slider[i].barRect.y - (menu->slider[i].sliderRect.h / 4);
    if( menu->slider[i].sliderStartVal )
    {
        if( menu->slider[i].sliderStartVal == FarRight )
        { menu->slider[i].sliderRect.x = menu->slider[i].barRect.x
          + menu->slider[i].barRect.w - (menu->slider[i].sliderRect.w / 2); }
         else
         {
             menu->slider[i].sliderRect.x = ( (menu->slider[i].barRect.w / (menu->slider[i].numValues - 1))
                            * menu->slider[i].sliderStartVal ) + menu->slider[i].barRect.x
                            - (menu->slider[i].sliderRect.w / 2);
         }
    }
    else
    { menu->slider[i].sliderRect.x = menu->slider[i].barRect.x - (menu->slider[i].sliderRect.w / 2); }
    yoff = save;
}

/* TODO: allow for vertical sliders to be able to be placed on left side of cell or right side of cellWidth.
this would include adjusting the "xoff" value accoding to the specific slider configuration
*/
static inline void setDefaultVerticalSliderPos( Menu* menu, int xoff, int yoff, int i )
{
    int cellWidth = menu->grid->cellWidth_px;
    int cellHeight = menu->grid->cellHeight_px;
    int save = xoff;
    // TODO: Keep this as a hard coded value? NOTE: gridStartLoc.h uses int only :D
    menu->slider[i].barRect.w = cellWidth * 0.125;
    menu->slider[i].barRect.h = cellHeight * menu->slider[i].gridStartLoc.h - 2 * xoff;

    // TODO: Make this pretty, and consistent with different menu sizes (the dependencees are non-consistent)
    if( menu->slider[i].alignment )
    { xoff += cellWidth - 3 * xoff; }

    menu->slider[i].barRect.x = menu->slider[i].gridStartLoc.x * cellWidth + xoff;
    menu->slider[i].barRect.y = menu->slider[i].gridStartLoc.y * cellHeight + yoff;

    menu->slider[i].sliderRect.w = menu->slider[i].barRect.w * 2;
    menu->slider[i].sliderRect.h = 20; // Keep this value?
    menu->slider[i].sliderRect.x = menu->slider[i].barRect.x - (menu->slider[i].sliderRect.w / 4);
    //menu->slider[i].sliderRect.y = menu->slider[i].barRect.y - (menu->slider[i].sliderRect.h / 4);
    if( menu->slider[i].sliderStartVal )
    {
        if( menu->slider[i].sliderStartVal == FarRight )
        { menu->slider[i].sliderRect.y = menu->slider[i].barRect.y
          + menu->slider[i].barRect.h - (menu->slider[i].sliderRect.h / 2); }
         else
         {
             menu->slider[i].sliderRect.y = ( (menu->slider[i].barRect.h / (menu->slider[i].numValues - 1))
                            * menu->slider[i].sliderStartVal ) + menu->slider[i].barRect.y
                            - (menu->slider[i].sliderRect.h / 2);
         }
    }
    else
    { menu->slider[i].sliderRect.y = menu->slider[i].barRect.y - (menu->slider[i].sliderRect.h / 2); }
    xoff = save;
}

static inline void DrawSliders( SDL_Renderer* rend, Menu* menu, int xoff, int yoff )
{
    int i;
    for(i = 0; i < menu->numSliders; i++)
    {
        if( menu->slider[i].defaultNeedsDrawn )
        {
            if( menu->slider[i].sliderOrientation )
            { setDefaultVerticalSliderPos( menu, xoff, yoff, i ); }
            else
            { setDefaultHorizontalSliderPos( menu, xoff, yoff, i ); }
            menu->slider[i].defaultNeedsDrawn = 0;
        }
        SDL_SetRenderDrawColor( rend, 255, 255, 255, SDL_ALPHA_OPAQUE);
        SDL_RenderFillRect( rend, &menu->slider[i].barRect );
        if( !menu->slider[i].state.isSelected && !menu->slider[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 255, 0, 0, SDL_ALPHA_OPAQUE);
        }
        else if( menu->slider[i].state.isHovered )
        {
            SDL_SetRenderDrawColor( rend, 0, 255, 0, SDL_ALPHA_OPAQUE );
        }
        else if( menu->slider[i].state.isSelected )
        {
            SDL_SetRenderDrawColor( rend, 0, 0, 255, SDL_ALPHA_OPAQUE );
        }
        SDL_RenderFillRect( rend, &menu->slider[i].sliderRect );
        if( menu->slider[i].state.isSelected )
        {
            if( menu->slider[i].sliderOrientation )
            { DrawVerticalSliderValues( rend, i, menu ); }
            else
            { DrawHorizontalSliderValues( rend, i, menu ); }
        }
    }
}

static inline int IsWithin( SDL_Rect rect, int x, int y )
{
    if( rect.x <= x && rect.x + rect.w >= x && rect.y <= y && rect.y + rect.h >= y )
    { return 1; }
    else
    { return 0; }
}

static inline void UpdateButtons( Menu* menu, int mousex, int mousey, MouseState mouseState )
{
    int i;
    for(i = 0; i < menu->numButtons; i++)
    {
        if( IsWithin( menu->button[i].buttonRect, mousex, mousey ) )
        {
            menu->button[i].state.isHovered = 1;
            if ( mouseState == MouseEventUp || mouseState == MouseUp )
            { menu->button[i].state.isSelected = 0; }

            if ( menu->button[i].state.isSelected || mouseState == MouseEventDown )
            {
                menu->button[i].state.isSelected = 1;
                menu->button[i].state.isHovered = 0;
            }
        }
        else
        {
            menu->button[i].state.isHovered = 0;
            menu->button[i].state.isSelected = 0;
        }
    }
}

static inline void UpdateCheckboxes( Menu* menu, int mousex,int mousey,
                                                  MouseState mouseState )
{
    int i;
    for(i = 0; i < menu->numCheckboxes; i++)
    {
        if( IsWithin( menu->checkbox[i].checkboxRect, mousex, mousey ) )
        {
            menu->checkbox[i].state.isHovered = 1;
            if( mouseState == MouseEventUp || mouseState == MouseUp )
            { menu->checkbox[i].state.isSelected = 0; }
            //else if?
            if( menu->checkbox[i].state.isSelected || mouseState == MouseEventDown )
            {
                menu->checkbox[i].state.isSelected = 1;
                menu->checkbox[i].state.isHovered = 0;
            }
        }
        else
        {
            menu->checkbox[i].state.isHovered = 0;
            menu->checkbox[i].state.isSelected = 0;
        }
    }
}

// TODO: make the int i's and int mousex's const int* and change func call to pass in address
//do this for all functions where values do not get changed
static inline void SnapToHorizontalValue( Menu* menu, int i, int mousex )
{
    int j;
    int xPos;
    int xPosNext;
    for(j = 0; j < menu->slider[i].numValues; j++)
    {
        xPos = ( (menu->slider[i].barRect.w * j) / (menu->slider[i].numValues - 1) )
                                                      + menu->slider[i].barRect.x;
        // If we still have a NEXT value
        if( j < menu->slider[i].numValues - 1 )
        {
            xPosNext = ( (menu->slider[i].barRect.w * (j + 1)) / (menu->slider[i].numValues - 1) )
                                                          + menu->slider[i].barRect.x;
            if( mousex > ((xPosNext + xPos) / 2) && mousex < xPosNext )
            { menu->slider[i].sliderRect.x = xPosNext - (menu->slider[i].sliderRect.w / 2); }
            else if( mousex < xPosNext && j == 0 )
            { menu->slider[i].sliderRect.x = xPos - (menu->slider[i].sliderRect.w / 2 ); }
        }
    }
}

static inline void SnapToVerticalValue( Menu* menu, int i, int mousey )
{
    int j;
    int yPos;
    int yPosNext;
    for(j = 0; j < menu->slider[i].numValues; j++)
    {
        yPos = ( (menu->slider[i].barRect.h * j) / (menu->slider[i].numValues - 1) )
                                                      + menu->slider[i].barRect.y;
        // If we still have a NEXT value
        if( j < menu->slider[i].numValues - 1 )
        {
            yPosNext = ( (menu->slider[i].barRect.h * (j + 1)) / (menu->slider[i].numValues - 1) )
                                                          + menu->slider[i].barRect.y;
            if( mousey > ((yPosNext + yPos) / 2) && mousey < yPosNext )
            { menu->slider[i].sliderRect.y = yPosNext - (menu->slider[i].sliderRect.h / 2); }
            else if( mousey < yPosNext && j == 0 )
            { menu->slider[i].sliderRect.y = yPos - (menu->slider[i].sliderRect.h / 2 ); }
        }
    }
}

// TODO: DELETE
/*static inline void DrawVerticalSlider( Menu* menu, int i )
{
    int holdVal;

    holdVal = menu->slider[i].barRect.w;
    menu->slider[i].barRect.w = menu->slider[i].barRect.h;
    menu->slider[i].barRect.h = holdVal;
}*/

static inline void UpdateSliders( Menu* menu, int mousex, int mousey,
                            MouseState mouseState )//TODO: need this?->, int yoff )
{
    int i;
    for(i = 0; i < menu->numSliders; i++)
    {
        if( IsWithin( menu->slider[i].sliderRect, mousex, mousey ) )
        {
            menu->slider[i].state.isHovered = 1;
            if( mouseState == MouseEventUp || mouseState == MouseUp )
            { menu->slider[i].state.isSelected = 0; }
            //else if?
            if( menu->slider[i].state.isSelected || mouseState == MouseEventDown )
            {
                menu->slider[i].state.isSelected = 1;
                menu->slider[i].state.isHovered = 0;
            }
        }
        // Case it is not within the slider rectangle but still selected
        else if( menu->slider[i].state.isSelected && mouseState == MouseDown )
        { menu->slider[i].state.isSelected = 1; }
        else
        {
            menu->slider[i].state.isHovered = 0;
            menu->slider[i].state.isSelected = 0;
            if( IsWithin( menu->slider[i].barRect, mousex, mousey ) && (mouseState == MouseEventDown) )
            { menu->slider[i].state.isSelected = 1; }
        }
        if( menu->slider[i].state.isSelected )
        {
            if( menu->slider[i].sliderOrientation )
            {
                if( mousey > menu->slider[i].barRect.y &&
                    mousey < (menu->slider[i].barRect.y + menu->slider[i].barRect.h) )
                { SnapToVerticalValue( menu, i, mousey ); }
            }
            else
            {
                if( mousex > menu->slider[i].barRect.x &&
                    mousex < (menu->slider[i].barRect.x + menu->slider[i].barRect.w) )
                { SnapToHorizontalValue( menu, i, mousex ); }
            }
        }
    }
}

void UpdateMenu( Menu* menu, int mousex, int mousey, MouseState mouseState )
{
    UpdateButtons( menu, mousex, mousey, mouseState );
    UpdateCheckboxes( menu, mousex, mousey, mouseState );
    UpdateSliders( menu, mousex, mousey, mouseState );
}

void DisplayMenu( SDL_Renderer* rend, Menu* menu, int showGrid )
{
    SDL_RenderClear( rend) ;
    if( showGrid )
    { DrawGrid( menu, rend ); }

    // TODO: make this not bit wise op, and make this generic (same for all menus, i.e. variable driven)
    DrawButtons( rend, menu, 0x8 << 1, 0x8 << 1 );

    DrawCheckboxes( rend, menu, 0x8 << 1, 0x8 << 1 );

    DrawSliders( rend, menu, 0x8 << 1, 0x8 << 1 );
}

void DestroyMenu( Menu* menu )
{
    // TODO: Free everything. LEAK IN MEMORY ATM :D all c/allocs get a free

    free( menu );

#ifdef DEBUG
    applog("Destroyed a Menu...");
#endif

}
