#include "stdio.h"
#include "SDL.h"
#include "Menu.h"

#include "AppState.h"

void PrepareMenu( Menu* menu );

int main (int argc, char** argv )
{

    applog("Initializing SDL...");

    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        applog( "Failed to initialize...");
        return -1;
    }

    applog( "Creating window and renderer..." );
    SDL_Window* window;
    SDL_Renderer* winrend;
    if ( SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN, &window, &winrend) < 0 ) {
        applog( "Failed to create window and renderer..." );
        return -1;
    }

    // Try without the below? comment out
    SDL_SetRenderDrawBlendMode(winrend, SDL_BLENDMODE_BLEND);

    SDL_Color color;
    color.r = 0;
    color.g = 40;
    color.b = 70;

    FNT_Font* font = FNT_InitFont(winrend, "rsc/540x20Font.bmp", "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 3, 4, color);

    Menu* menu = CreateMenu( MENUWIDTH , MENUHEIGHT, font );

    PrepareMenu( menu );

    SDL_Event event;

    do{
        SDL_WaitEvent( &event );
        SDL_SetRenderDrawColor( winrend, 0, 0, 0, SDL_ALPHA_OPAQUE );

        MouseState state;
        int buttondown, mousex, mousey;
        buttondown = SDL_GetMouseState( &mousex, &mousey );
        if( buttondown )
        { state = MouseDown; }
        else
        { state = MouseUp; }

        switch( event.type )
        {
            case SDL_MOUSEBUTTONDOWN:
                state = MouseEventDown;
                break;
            case SDL_MOUSEBUTTONUP:
                state = MouseEventUp;
                break;
        }

        UpdateMenu( menu, mousex, mousey, state );

        DisplayMenu( winrend, menu, 1 );

        SDL_RenderPresent( winrend );

    }while( event.type != SDL_QUIT );

    DestroyMenu( menu );
    FNT_DestroyFont( font );

    return 0;
}


void PrepareMenu( Menu* menu )
{
    int widget_cells_wide = 3;
    int widget_cells_height = 2;
    int widget_loc_x = 2;
    int widget_loc_y = 2;

    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );
    widget_loc_y += 2;
    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );
    widget_loc_y += 2;
    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );
    widget_loc_x += 3;
    widget_loc_y = 2;
    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );
    widget_loc_y += 2;
    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );
    widget_loc_y += 2;
    AddButton( menu, widget_cells_wide, widget_cells_height, widget_loc_x, widget_loc_y, "TEST" );


    int checkboxSideLength = 1;
    widget_loc_x = 1;
    widget_loc_y = 2;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Right );

    widget_loc_x += 7;
    widget_loc_y = 2;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );
    widget_loc_y += 1;
    AddCheckbox( menu, checkboxSideLength, widget_loc_x, widget_loc_y, Left );

    //AddSlider(Menu, int cellsSpan_Bar, int numValues, int sliderStartVal,
    //           int gridLoc_x, int gridLoc_y, int sliderOrientation, int alignment)
    AddSlider( menu, 6, 20, FarLeft, 2, 8, Horizontal, Up );
    AddSlider( menu, 6, 15, FarRight, 2, 1, Horizontal, Down );
    AddSlider( menu, 6, 5, Top, 1, 2, Vertical, Left );
    AddSlider( menu, 6, 10, Bottom, 8, 2, Vertical, Right );
}
